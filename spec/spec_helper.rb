require 'benchmark'
benchmark_format = "%n\t#{Benchmark::FORMAT}"

puts Benchmark.measure('app') {
  lib = File.expand_path('../../lib', __FILE__)
  require File.join(lib, 'peppers-ghost')
}.format(benchmark_format)

puts Benchmark.measure('specs') {
  Dir['spec/support/**/*.rb'].each do |file|
    require File.expand_path(file)
  end

  RSpec.configure do |config|
    config.order = :random
    Kernel.srand config.seed
  end
}.format(benchmark_format)
