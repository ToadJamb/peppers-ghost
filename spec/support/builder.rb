module PeppersGhostSpecHelper
  class Builder
    def initialize
      @hash = {}
      @obj = Class.new.new
    end

    def add_attribute(name, value)
      if value.is_a?(Array)
        hash_value = []
        obj_value = []

        value.each do |val|
          hash_value << hash_value_for(val)
          obj_value << obj_value_for(val)
        end
      else
        hash_value = hash_value_for(value)
        obj_value = obj_value_for(value)
      end

      set_hash_value name, hash_value
      set_obj_value name, obj_value

      self
    end

    def to_hash
      @hash
    end

    def to_object
      @obj
    end

    private

    def set_hash_value(name, value)
      @hash[name.to_s] = value
    end

    def set_obj_value(name, value)
      add_obj_attr(name) unless @obj.respond_to?(name)
      @obj.send "#{name}=", value
    end

    def add_obj_attr(name)
      new_attr = "attr_accessor :#{name}"
      @obj.class.class_eval do
        eval new_attr
      end
    end

    def hash_value_for(value)
      return value.to_hash if value.is_a?(PeppersGhostSpecHelper::Builder)
      value
    end

    def obj_value_for(value)
      return value.to_object if value.is_a?(PeppersGhostSpecHelper::Builder)
      value
    end
  end
end
