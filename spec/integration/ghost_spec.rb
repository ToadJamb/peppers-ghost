require 'spec_helper'

describe PeppersGhost do
  before do
    PeppersGhost::Ghosts.instance_variable_set(:@ghosts, {})

    PeppersGhost.define do
      ghost :gfoo do
        id { 34 }
        baz { 'bazzer' }

        ghost :gbar do
          id { 35 }
          qux { 'quxxer' }

          ghost :gbaz do
            id
          end
        end

        ghost :gfizz do
          id { 21 }
          id { 42 }
        end
      end
    end
  end

  describe '.ghost' do
    shared_examples 'attribute definition' do |name, attr, type, value|
      let(:ghost) { PeppersGhost::Ghosts.find name }
      let(:attribute) { ghost.attribute_definition_for attr }
      let(:attr_class) do
        case type
        when :dynamic then PeppersGhost::DynamicAttributeDefinition
        when :implicit then PeppersGhost::ImplicitAttributeDefinition
        else
          NilClass
        end
      end
      let(:ghost_value) { attribute.evaluate ghost }

      it "#{name} is a ghost" do
        expect(ghost).to be_a PeppersGhost::Ghost
      end

      it "#{name}.#{attr} is a #{type || 'nil'} attribute" do
        expect(attribute).to be_a attr_class
      end

      it "#{name}.#{attr} evaluates to #{value}" do
        expect(ghost_value).to eq value if type == :dynamic
      end
    end

    it_behaves_like 'attribute definition', :gfoo, :id, :dynamic, 34
    it_behaves_like 'attribute definition', :gfoo, :baz, :dynamic, 'bazzer'
    it_behaves_like 'attribute definition', :gfoo, :qux

    it_behaves_like 'attribute definition', :gbar, :id, :dynamic, 35
    it_behaves_like 'attribute definition', :gbar, :baz, :dynamic, 'bazzer'
    it_behaves_like 'attribute definition', :gbar, :qux, :dynamic, 'quxxer'

    it_behaves_like 'attribute definition', :gbaz, :id, :implicit
    it_behaves_like 'attribute definition', :gbaz, :baz, :dynamic, 'bazzer'
    it_behaves_like 'attribute definition', :gbaz, :qux, :dynamic, 'quxxer'

    it_behaves_like 'attribute definition', :gfizz, :id, :dynamic, 42
    it_behaves_like 'attribute definition', :gfizz, :baz, :dynamic, 'bazzer'
    it_behaves_like 'attribute definition', :gfizz, :qux

    context 'given a duplicate ghost' do
      let(:ghost_definition) do
        PeppersGhost.define do
          ghost :gfoo do
            id
          end
        end
      end

      it 'raises an error' do
        expect { ghost_definition }
          .to raise_error PeppersGhost::DuplicateGhostException
        expect { ghost_definition }
          .to raise_error 'Duplicate ghost definition: gfoo'
      end
    end
  end
end
