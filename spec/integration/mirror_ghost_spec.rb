require 'spec_helper'

describe 'PeppersGhost::Matchers.mirror_ghost' do
  subject { mirror_ghost ghost_name }

  let(:ghost_name) { :thing }

  let(:reference) { reference_base }
  let(:thing)     { obj_base }

  let(:source) { [thing_builder, builder_list].sample }

  let(:obj_base) do
    if source.is_a?(Array)
      source.map(&:to_object)
    else
      source.to_object
    end
  end

  let(:reference_base) do
    if source.is_a?(Array)
      source.map(&:to_hash)
    else
      source.to_hash
    end
  end

  let(:builder_list) do
    arr = []
    rand(1..5).times do
      arr << build_thing
    end
    arr
  end

  let(:thing_builder) { build_thing }

  def build_thing
    PeppersGhostSpecHelper::Builder.new.add_attribute(:id, rand(1..999))
      .add_attribute(:foo, 'bar')
      .add_attribute(:baz, ['abc', 'def', nil].sample)
      .add_attribute(:sub, build_thur)
      .add_attribute(:subber, build_thars)
  end

  def build_thur
    PeppersGhostSpecHelper::Builder.new.add_attribute(:id, rand(1..999))
      .add_attribute(:bus, 'qux')
      .add_attribute(:zip, ['xyz', 'def', nil].sample)
  end

  def build_thar
    PeppersGhostSpecHelper::Builder.new.add_attribute(:id, rand(1..999))
      .add_attribute(:bar, ['ghi', 'jkl'].sample)
      .add_attribute(:qux, ['abc', 'xyz', nil].sample)
  end

  def build_thars
    arr = []
    rand(1..5).times do
      arr << build_thar
    end
    arr
  end

  before { PeppersGhost::Ghosts.instance_variable_set :@ghosts, {} }
  after { PeppersGhost::Frogger.instance_variable_set :@frog, {} }

  before do
    PeppersGhost.define do
      ghost :thur do
        id
        bus { 'qux' }
        zip
      end

      ghost :thar do
        id
        bar
        qux
      end

      ghost :thing do
        id
        foo { 'bar' }
        baz
        sub(:ghost => :thur) { respond_to?(:sub) ? sub : send(:[], 'sub') }
        subber :ghosts => :thar
      end
    end
  end

  shared_examples 'a failed mirror' do |regexs|
    regexs.each do |regex|
      it "has a failure message matching #{regex}" do
        subject.matches?(reference)
        expect(subject.failure_message).to match regex
      end
    end
  end

  shared_examples 'a successful mirror' do
    it 'passes' do
      expect(subject.matches?(reference)).to eq(true),
        subject.failure_message
    end
  end

  context 'by default' do
    it_behaves_like 'a successful mirror'
  end

  context 'given a singular reference' do
    let(:source) { thing_builder }

    context 'by default' do
      it_behaves_like 'a successful mirror'

      context 'given a missing attribute' do
        before { reference.delete 'foo' }

        it_behaves_like 'a failed mirror', [
          /missing elements were: +\["foo"\]/,
        ]
      end

      context 'given an extra attribute' do
        before { reference['extra'] = 'flavor' }
        it_behaves_like 'a failed mirror', [
          /extra elements were: +\["extra"\]/,
        ]
      end

      context 'given an extra attribute in a child' do
        before { reference['subber'][0]['extra'] = 'flavor' }
        it_behaves_like 'a failed mirror', [
          /extra elements were: +\["extra"\]/,
          /thing\.subber\[0\] does not match/,
        ]
      end

      context 'given a single child is an array' do
        before { reference['sub'] = [reference['sub']] }
        it_behaves_like 'a failed mirror', [
          /Did not expect thing\.sub to be an array/,
        ]
      end

      context 'given a child array is singular' do
        before { reference['subber'] = reference['subber'].first }
        it_behaves_like 'a failed mirror', [
          /Expected thing\.subber to be an array/,
        ]
      end

      context 'given an incorrect value' do
        before { thing.id = 37 }
        before { reference['id'] = 3 }
        it_behaves_like 'a failed mirror', [
          /expected: 37\n *got: 3/,
          /thing\.id does not match/,
        ]
      end

      context 'given an incorrect nested value' do
        before { thing.sub.id = 24 }
        before { reference['sub']['id'] = 4 }
        it_behaves_like 'a failed mirror', [
          /expected: 24\n *got: 4/,
          /thing\.sub\.id does not match/,
        ]
      end
    end

    context 'given strict checking' do
      subject { mirror_ghost(ghost_name).strictly }

      let(:subbers) { [] }
      let(:sub) { build_thur.add_attribute :zip, 'eog' }

      before { thing_builder.add_attribute :baz, 'efo' }
      before { thing_builder.add_attribute :subber, subbers }
      before { thing_builder.add_attribute :sub, sub }

      context 'given a nil value in the ghost' do
        let(:reference) {{ 'id' => 34 }}
        let(:ghost_name) { :foo }
        let(:foo) { Struct.new(:id).new 48 }

        before do
          PeppersGhost.define do
            ghost :foo do
              id { nil }
            end
          end
        end

        it_behaves_like 'a failed mirror', [
          /foo\.id is nil/,
        ]
      end

      context 'given a nil value on both sides' do
        before { reference['baz'] = nil }
        before { thing.baz = nil }

        it_behaves_like 'a failed mirror', [
          /thing\.baz is nil/,
        ]
      end

      context 'given a nested nil value in an array' do
        let(:subbers) { [build_thar.add_attribute(:qux, 'lgr'), subber] }
        let(:subber) { build_thar.add_attribute :qux, nil }

        it_behaves_like 'a failed mirror', [
          /thing\.subber\[1\].qux is nil/,
        ]
      end

      context 'given a nested nil value in a singular' do
        let(:sub) { build_thur.add_attribute :zip, nil }

        before { thing_builder.add_attribute :sub, sub }

        it_behaves_like 'a failed mirror', [
          /thing\.sub.zip is nil/,
        ]
      end
    end
  end

  context 'given a nil reference' do
    context 'given it is the initial reference' do
      let(:reference) { nil }

      it_behaves_like 'a failed mirror', [
        /thing is not expected to be nil/,
      ]
    end

    context 'given it is a nested array' do
      let(:source) { thing_builder }

      before { thing_builder.add_attribute(:subber, [build_thar, build_thar]) }
      before { reference['subber'][1] = nil }

      it_behaves_like 'a failed mirror', [
        /thing\.subber\[1\] is not expected to be nil/,
      ]
    end
  end

  context 'given the reference is an array' do
    let(:source) { builder_list }

    context 'by default' do
      it_behaves_like 'a successful mirror'
    end

    context 'given too many items in the primitive' do
      let(:builder_list) { [build_thing, build_thing] }
      let(:reference) {[
        builder_list.first.to_hash,
        builder_list.last.to_hash,
        build_thing.to_hash,
      ]}

      it_behaves_like 'a failed mirror', [
        /Expected thing to have 2 items, found 3/,
      ]
    end

    context 'given too many items in a nested attribute' do
      before do
        builder_list.first.add_attribute :subber, [
          build_thar, build_thar, build_thar
        ]
      end

      before { reference.first['subber'] << build_thar }

      it_behaves_like 'a failed mirror', [
        /Expected thing.subber to have 3 items, found 4/,
      ]
    end
  end

  context 'given a specified object' do
    subject { mirror_ghost(ghost_name).for object }
    let(:object) { obj_base }
    let(:thing) { {:foo => 'bar'} }
    it_behaves_like 'a successful mirror'
  end

  context 'given the object is a hash' do
    let(:thing) { reference_base }
    it_behaves_like 'a successful mirror'
  end
end
