require 'spec_helper'

describe PeppersGhost do
  it 'does not include core methods in the RSpec namespace' do
    expect(PeppersGhost::MirrorHelper.respond_to?(:mirrors_ghost)).to eq true

    expect{mirrors_ghost}.to raise_error NameError
    expect{mirrors_ghost}.to raise_error(/mirrors_ghost/)
  end
end
