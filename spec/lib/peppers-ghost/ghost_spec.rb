require 'spec_helper'

describe PeppersGhost::Ghost do
  subject { described_class.new ghost_name, parent }

  let(:ghost_name) { [:foo, 'foo'].sample }
  let(:parent) { [described_class.new(parent_name, nil), nil].sample }
  let(:parent_name) { [:bar, 'bar'].sample }

  describe '#new' do
    shared_examples 'it has a string name' do |name|
      let(:ghost_name) { name }
      context "given #{name.inspect}" do
        it "has a name of #{name.to_s}" do
          expect(subject.ghost_name).to eq name.to_s
        end
      end
    end

    it_behaves_like 'it has a string name', :foo
    it_behaves_like 'it has a string name', 'foo'
  end

  describe '.canonical_name_for' do
    subject { described_class.canonical_name_for ghost_name }

    shared_examples 'a canonical ghost name' do |given, expected|
      let(:ghost_name) { given }
      context "given #{given.inspect}" do
        it "returns #{expected.inspect}" do
          expect(subject).to eq expected
        end
      end
    end

    it_behaves_like 'a canonical ghost name', 'foo', 'foo'
    it_behaves_like 'a canonical ghost name', :foo, 'foo'
    it_behaves_like 'a canonical ghost name', 'fOo', 'foo'
    it_behaves_like 'a canonical ghost name', :fOo, 'foo'
  end
end
