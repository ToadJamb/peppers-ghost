require 'spec_helper'

describe PeppersGhost::Ghosts do
  let(:ghost_name) { base_name }
  let(:base_name) { [:foo, :bar].sample.send([:to_s, :to_sym].sample) }

  before { described_class.instance_variable_set :@ghosts, {} }

  describe '.find' do
    subject { described_class.find ghost_name, true }

    shared_examples 'a ghost not found' do
      it 'raises a ghost not found exception' do
        expect{subject}.to raise_error PeppersGhost::GhostNotFoundException
      end
    end

    shared_examples 'a found ghost' do |original_name, query|
      let!(:ghost) do
        described_class.add PeppersGhost::Ghost.new(original_name, nil)
      end

      let(:ghost_name) { query }

      context "given the original name is #{original_name.inspect}" do
        context "given a search for #{query.inspect}" do
          it 'returns the ghost' do
            expect(subject).to eq ghost
          end
        end
      end
    end

    context 'given the ghost exists' do
      it_behaves_like 'a found ghost', :foo, :foo
      it_behaves_like 'a found ghost', 'foo', 'foo'
      it_behaves_like 'a found ghost', :foo, 'foo'
      it_behaves_like 'a found ghost', 'foo', :foo
    end

    context 'given the ghost does not exist' do
      context 'given a string name' do
        let(:ghost_name) { base_name.to_s }
        it_behaves_like 'a ghost not found'
      end

      context 'given a symbol name' do
        let(:ghost_name) { base_name.to_sym }
        it_behaves_like 'a ghost not found'
      end
    end
  end
end
