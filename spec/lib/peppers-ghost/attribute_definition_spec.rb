require 'spec_helper'

describe PeppersGhost::ImplicitAttributeDefinition do
  let(:instance) { described_class.new attr_name }

  let(:attr_name) { [:foo_attr, 'bar_attr'].sample }

  describe '#evaluate' do
    subject { instance.evaluate object }

    let(:object) { Object.new }

    context 'given a hash' do
      let(:object) { hash }

      shared_examples 'implicit hash attribute' do |key, value, attr, expected|
        let(:hash) { {key => value} }
        let(:attr_name) { attr }

        context "given #{key.inspect} => #{value.inspect}" do
          it "returns #{expected.inspect}" do
            expect(subject).to eq expected
          end
        end
      end

      it_behaves_like 'implicit hash attribute', :foo, :bar, :foo, :bar
      it_behaves_like 'implicit hash attribute', 'foo', :bar, :foo, :bar
      it_behaves_like 'implicit hash attribute', :foo, :bar, 'foo', :bar

      it_behaves_like 'implicit hash attribute', :foo, true, :foo, true
      it_behaves_like 'implicit hash attribute', 'foo', true, :foo, true
      it_behaves_like 'implicit hash attribute', :foo, true, 'foo', true

      it_behaves_like 'implicit hash attribute', :foo, nil, :foo, nil
      it_behaves_like 'implicit hash attribute', 'foo', nil, :foo, nil
      it_behaves_like 'implicit hash attribute', :foo, nil, 'foo', nil

      it_behaves_like 'implicit hash attribute', :foo, false, :foo, false
      it_behaves_like 'implicit hash attribute', 'foo', false, :foo, false
      it_behaves_like 'implicit hash attribute', :foo, false, 'foo', false
    end
  end
end
