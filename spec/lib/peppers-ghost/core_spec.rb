require 'spec_helper'

describe PeppersGhost::Core do
  let(:ghost_name) { base_name }
  let(:base_name) { [:foo, :bar].sample.send([:to_s, :to_sym].sample) }

  before { PeppersGhost::Ghosts.instance_variable_set :@ghosts, {} }

  describe '.ghost' do
    subject { described_class.ghost ghost_name }

    context 'given a duplicate name is given' do
      shared_examples 'a duplicate name' do |original, duplicate|
        context "given the original name is #{original.inspect}" do
          before { described_class.ghost original }

          context "given a duplicate name of #{duplicate.inspect}" do
            let(:ghost_name) { duplicate }

            it 'raises an error' do
              expect{subject}
                .to raise_error PeppersGhost::DuplicateGhostException
            end
          end
        end
      end

      it_behaves_like 'a duplicate name', :foo, :foo
      it_behaves_like 'a duplicate name', 'foo', 'foo'
      it_behaves_like 'a duplicate name', 'foo', :foo
      it_behaves_like 'a duplicate name', :foo, 'foo'
    end
  end
end
