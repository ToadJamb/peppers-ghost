require 'rspec'
require 'uuidtools'

module PeppersGhost
end

require_relative 'peppers-ghost/mirror_helper'
require_relative 'peppers-ghost/exceptions'
require_relative 'peppers-ghost/frogger'
require_relative 'peppers-ghost/leaf'
require_relative 'peppers-ghost/attribute_definition'
require_relative 'peppers-ghost/ghost'
require_relative 'peppers-ghost/ghosts'
require_relative 'peppers-ghost/core'

require_relative 'peppers-ghost/matchers'
require_relative 'peppers-ghost/matchers/mirror_ghost'

module PeppersGhost
  extend Core
end

RSpec.configure do |config|
  config.include PeppersGhost::Matchers
end
