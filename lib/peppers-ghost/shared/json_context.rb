shared_context 'json' do
  let(:json)      { JSON last_response.body }
  let(:root_key)  { json.keys.reject { |k| k == 'meta' }.first }
  let(:json_root) { json[root_key] }
end
