module PeppersGhost
  module Frogger
    extend self

    def log_ghost(definition, exists)
      ghost = definition.ghost || definition.ghosts
      ghost_name = ghost.ghost_name

      frog[ghost_name] ||= 0
      frog[ghost_name] += 1 if exists
    end

    def frog
      @frog ||= {}
    end

    def eat_the_fly!
      untested, tested = frog.partition do |key, value|
        value == 0
      end

      tested = tested.sort_by(&:last).reverse

      if untested.any?
        puts formatted_message(tested, untested)
        burp untested
      end
    end

    def burp(untested)
      pluralized = 'ghost'.pluralize(untested.count)
      untested_list = untested.map(&:first).join(', ')

      raise "Untested nested #{pluralized}: #{untested_list}."
    end

    def formatted_message(tested, untested)
      "\n\nTested: %s\nUntested: %s" % [
        formatted_ghosts(tested),
        formatted_ghosts(untested),
      ]
    end

    def formatted_ghosts(list)
      mask = '%s (%d)'
      list.map { |ghost_ref| mask % ghost_ref }.join(', ')
    end
  end
end

RSpec.configure do |config|
  config.after(:suite) do
    PeppersGhost::Frogger.eat_the_fly!
  end
end
