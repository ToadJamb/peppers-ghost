module PeppersGhost
  module MirrorHelper
    extend self

    def mirrors_ghost(actual, expected, ghost, attr, strict = false)
      @leaf = Leaf.new(ghost, @leaf, attr) unless @leaf.index
      match_structure actual, expected, ghost
      match_values actual, expected, ghost, strict
    end

    private

    def match_structure(actual, expected, ghost)
      expect(actual).to_not eq(nil),
        "#{@leaf.name} is not expected to be nil"

      if actual.is_a?(Array)
        check_count actual, expected, ghost
        match_single_structure actual, expected, ghost
      else
        attributes = ghost.attribute_names
        match_keys actual.keys, attributes.map(&:to_s)
      end
    end

    def match_keys(keys, attributes)
      begin
        expect(keys).to match_array attributes
      rescue RSpec::Expectations::ExpectationNotMetError => e
        message = "#{@leaf.name} does not match the expected structure.\n"
        message += e.message
        raise RSpec::Expectations::ExpectationNotMetError, message
      end
    end

    def match_values(actual, expected, ghost, strict = false)
      if actual.is_a?(Array)
        actual.each_with_index do |hash, i|
          match_values hash, expected[i], ghost, strict
        end
      else
        match_attributes actual, expected, ghost, strict
      end
    end

    def match_single_structure(hashes, references, ghost)
      hashes.each_with_index do |hash, i|
        match_structure hash, references[i], ghost
      end
    end

    def match_attributes(hash, reference, ghost, strict = false)
      ghost.attribute_names.each do |attribute|
        defn = ghost.attribute_definition_for(attribute)

        value      = defn.evaluate(reference)
        hash_value = hash[attribute.to_s]

        invoke_matcher_for ghost, attribute, defn, hash_value, value, strict
      end
    end

    def invoke_matcher_for(ghost, attr, defn, hash_value, value, strict = false)
      if defn.ghost || defn.ghosts then
        deep_match hash_value, value, defn, attr, strict
      else
        match_attribute ghost, attr, hash_value, value, strict
      end
    end

    def match_attribute(ghost, attr, hash_value, value, strict = false)
      if strict
        expect(value).to_not eq(nil),
          "#{@leaf.name}.#{attr} is nil.\n" +
        "Turn off strict mode for this test if a nil value is expected."
      end

      begin
        expect(hash_value).to eq value
      rescue RSpec::Expectations::ExpectationNotMetError => e
        message = "#{@leaf.name}.#{attr} does not match the expected value.\n"
        message += e.message
        raise RSpec::Expectations::ExpectationNotMetError, message
      end
    end

    def mirrors_many(hashes, references, ghost, attr, strict = false)
      @leaf = Leaf.new(ghost, @leaf, attr)
      check_count hashes, references, ghost
      hashes.each_with_index do |hash, i|
        @leaf.increment
        mirrors_ghost hash, references[i], ghost, attr, strict
      end
    end

    def check_count(hashes, references, ghost)
      expect(hashes).to be_an(Array),
        "Expected #{@leaf.name} to be an array."
      expect(references).to be_an Array
      expected_count hashes, references.count, ghost
    end

    def expected_count(hashes, count, ghost)
      expect(hashes.count).to eq(count),
        "Expected #{@leaf.name} to have #{count} items, " +
        "found #{hashes.count}"
    end

    def deep_match(hash_value, value, definition, attr, strict)
      valid = false

      if definition.ghosts
        valid = true if hash_value.count > 0
        deep_match_call :mirrors_many,
          hash_value, value, definition.ghosts, attr, strict
      else
        expect(hash_value).to_not be_an(Array),
          "Did not expect #{@leaf.name}.#{attr} to be an array."

        if hash_value or value
          valid = true
          deep_match_call :mirrors_ghost,
            hash_value, value, definition.ghost, attr, strict
        end
      end

      Frogger.log_ghost definition, valid
    end

    def deep_match_call(method, *args)
      send(method, *args)
      @leaf = @leaf.parent
    end
  end
end
