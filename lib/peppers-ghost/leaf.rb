module PeppersGhost
  class Leaf
    attr_reader :parent, :index

    def initialize(ghost = nil, parent = nil, attr = nil)
      @index = nil
      @parent = parent

      @name = nil
      if attr
        @name = attr
      else
        @name = ghost.ghost_name if ghost
      end
    end

    def name
      inc = "[#{@index}]" if @index
      parent_name = "#{@parent.name}." if @parent && @parent.name
      full_name = "#{parent_name}#{@name}#{inc}"
      return full_name unless full_name.empty?
    end

    def increment
      @index ||= -1
      @index += 1
    end
  end
end
