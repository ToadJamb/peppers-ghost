module PeppersGhost
  class BaseException < Exception
    def to_s
      @message || super
    end
  end

  class DuplicateGhostException < BaseException
    def initialize(ghost_name)
      @message = "Duplicate ghost definition: #{ghost_name}"
    end
  end

  class GhostNotFoundException < BaseException
    def initialize(ghost_name)
      @message = "Ghost not found: #{ghost_name}"
    end
  end
end
