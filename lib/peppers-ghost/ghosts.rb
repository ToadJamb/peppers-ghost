module PeppersGhost
  module Ghosts
    extend self

    def add(ghost)
      ghosts[ghost.ghost_name] = ghost
    end

    def find(ghost_name, raise_error = false)
      ghost = ghosts[Ghost.canonical_name_for(ghost_name)]
      raise GhostNotFoundException.new(ghost_name) if raise_error && !ghost
      ghost
    end

    private

    def ghosts
      @ghosts ||= {}
    end
  end
end
