module PeppersGhost
  class AttributeDefinition
    attr_reader :name, :ghost, :ghosts

    def initialize(name, options={})
      @name   = name
      @ghost  = get_ghost_for(options[:ghost])
      @ghosts = get_ghost_for(options[:ghosts])
    end

    private

    def get_ghost_for(ghost_name)
      return Ghosts.find(ghost_name) if ghost_name
    end

    def transform(value)
      case value
      when UUIDTools::UUID
        value.to_s
      when Date, Time
        value.iso8601
      else
        value
      end
    end
  end

  class ImplicitAttributeDefinition < AttributeDefinition
    def evaluate(object)
      if object.is_a?(Hash)
        value =
          if object[name.to_sym].is_a?(NilClass)
            object[name.to_s]
          else
            object[name.to_sym]
          end
        transform value
      elsif object.respond_to?(name)
        transform object.send(name)
      end
    end
  end

  class DynamicAttributeDefinition < AttributeDefinition
    def initialize(name, options={}, block)
      super name, options
      @block = block
    end

    def evaluate(object)
      transform object.instance_eval(&block)
    end

    private

    def block
      @block
    end
  end
end
