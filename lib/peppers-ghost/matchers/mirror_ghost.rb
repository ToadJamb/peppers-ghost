module PeppersGhost
  module Matchers
    matcher :mirror_ghost do |type|
      include MirrorHelper

      chain :strictly do
        @strict = true
      end

      chain :for do |expected|
        @expected = expected
      end

      match do |actual|
        @strict = false unless defined?(@strict)
        @error = nil

        begin
          @expected ||= send(type)
          ghost = Ghosts.find(type)
          @leaf = Leaf.new
          mirrors_ghost actual, @expected, ghost, nil, @strict
          true
        rescue RSpec::Expectations::ExpectationNotMetError => e
          @error = e.message
          false
        end
      end

      failure_message do
        @error
      end
    end
  end
end
