module PeppersGhost
  module Core
    extend self

    def define(&block)
      instance_eval &block
    end

    def ghost(name, parent = nil, &block)
      ghost = Ghost.new(name, parent)
      if Ghosts.find(ghost.ghost_name, false)
        raise DuplicateGhostException.new(name)
      end
      ghost.instance_eval(&block) if block_given?
      Ghosts.add ghost
    end
  end
end
