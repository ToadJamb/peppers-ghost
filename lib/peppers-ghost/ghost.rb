module PeppersGhost
  class Ghost
    class << self
      def canonical_name_for(name)
        name.to_s.downcase
      end
    end

    def initialize(name, parent)
      @attributes = (parent && parent.dup_attributes) || {}
      @name = self.class.canonical_name_for(name)
    end

    def ghost_name
      @name
    end

    def attribute(name, options={}, &block)
      if block_given?
        add_dynamic_attribute(name, options, &block)
      else
        add_implicit_attribute(name, options)
      end
    end

    def attribute_definition_for(name)
      @attributes[name]
    end

    def attribute_names
      @attributes.keys
    end

    def ghost(*args, &block)
      PeppersGhost.ghost(*args, self, &block)
    end

    protected

    def dup_attributes
      @attributes.dup
    end

    private

    def add_dynamic_attribute(name, options={}, &block)
      @attributes[name] = DynamicAttributeDefinition.new(name, options, block)
    end

    def add_implicit_attribute(name, options={})
      @attributes[name] = ImplicitAttributeDefinition.new(name, options)
    end

    def method_missing(name, *args, &block)
      attribute(name, *args, &block)
    end
  end
end
