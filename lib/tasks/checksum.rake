desc 'Generate a checksum for the current gem file'
task :checksum do |task|
  require 'digest/sha2'
  root = File.dirname(__FILE__)

  gem_file = Dir[File.join(root, '..', '..', '*.gem')].first
  checksum = Digest::SHA512.new.hexdigest(File.read(gem_file))
  checksum_file = File.basename(gem_file)
  checksum_path = "checksum/#{checksum_file}.sha512"

  puts checksum

  File.open(checksum_path, 'w') do |file|
    file.write checksum
  end
end
