TODO
====

* features
	* Provide a way to specify load order
	* Load ghosts automatically
	* evaluate nested ghosts after loading?...
		* this would ensure dependencies are resolved?...


Done
====

* Raise explicit ghost duplication exception
* integration spec for ghost matcher
* strict passes through to children
* check error messages
* remove structure matcher
* optionally pass in objects/arrays
* spec for hashes as the source object
* make ghost names strings and case-insensitive
* 'or' evaluations in attributes should be checking non-null
  (false is a valid value)
* remove references to json and resource
