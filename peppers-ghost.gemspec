Gem::Specification.new do |s|
  s.name    = 'peppers-ghost'
  s.version = '0.0.1'

  s.summary     = 'Adds an RSpec matcher for testing hash structures.'
  s.description = %Q{
    Adds an RSpec matcher that allows testing of nested hashes (or arrays of hashes) of any structure.
  }.strip

  s.author   = 'Travis Herrick'
  s.email    = 'tthetoad@gmail.com'
  s.homepage = 'http://www.bitbucket.org/ToadJamb/peppers-ghost'

  s.license = 'LGPLv3'

  s.extra_rdoc_files = Dir['README', 'license/*']

  s.require_paths = ['lib']
  s.files = Dir['*', 'lib/**/*.rb', 'license/*']
  s.test_files = Dir['spec/**/*.rb']

  s.add_dependency 'rspec'
  s.add_dependency 'uuidtools'

  s.add_development_dependency 'cane'

  s.has_rdoc = false
end
