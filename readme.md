Pepper's Ghost
==============

About
-----

Pepper's Ghost adds an RSpec matcher that allows you to explicitly test
complex hash structures (or arrays of hash structures).

Pepper's Ghost was born out of testing json apis,
but it is really capable of testing the contents of any hash.
The only restriction is that the keys of the hash under test
are required to be strings (due to the origins of testing parsed json).


Install
-------

	$ gem install 'peppers-ghost'


Basic Use
---------

The code in this basic example has been tested.
It will pass if pasted into a spec file.
That is a good place to start poking at what you can do.


### Ghost

	PeppersGhost.define do
	  ghost :foo do
	    id
	  end
	end


### Spec

	describe 'foo' do
	  let(:foo) { Struct.new(:id).new 3 }
	  let(:hash) { {'id' => 3} }
	
	  it 'tests foo' do
	    expect(hash).to mirror_ghost :foo
	  end
	end


Ghosts
------

Ghosts are defined using a dsl very similar to that of FactoryGirl.

The first thing to know is that the ghosts define the structure
of the expected hash(es). Every attribute defined must exist in the hash
as a key and no other keys can exist.


### Basic Usage

Ghosts are also used to determine what the expected value is.
This is done by calling a method
(or looking up the value in the case that the reference object is also a hash).
If the key in the expected hash
and the method on the reference object are the same,
then there is no need to do anything more than:

	ghost :foo do
	  id
	end


### Blocks

There are cases where a key may exist that is a computed value
which does not exist on the reference object.
In this case, a block may be passed.
The block will be executed in the context of the reference object.

Assuming a reference object of:

	Struct.new(:id, :first_name, :last_name).new 1, 'John', 'Doe'

And an expected hash resulting in:

	{ 'id' => 1, :full_name => 'John Doe' }

The following ghost would match:

	ghost :foo do
	  id
	  full_name { "#{first_name} #{last_name}" }
	end

This can be used to hard code values, if needed.
The following would also match the example structure and hash:

	ghost :foo do
	  id
	  full_name { 'John Doe' }
	end


### Adding/Updating one or two values

A Ghost may be defined in the context of another in order to override values.

	ghost :foo do
	  id
	  name

	  ghost :foo_bar
	    name { 'bar' }
	  end
	end

The ghost for foo_bar will always expect name to be 'bar',
while the foo ghost will expect name to be the name of the reference object.


### Nesting

#### Single

Ghosts may be nested in order to create complex structures:

	ghost :child do
	  id
	  bar
	end

	ghost :parent do
	  id
	  foo ghost: :child
	end

Given a reference object of:

	Struct.new(:id, :foo).new(3, Struct.new(:id, :bar).new(4, 'baz'))

This expects a hash that matches:

	{'id' => 3, 'foo' => {'id' => 4, 'bar' => 'baz'}}


#### Arrays

An array may be expected by pluralizing the ghost options key:

	ghost :child do
	  id
	  bar
	end

	ghost :parent do
	  id
	  foo ghosts: :child
	end

Now an array is expected instead of a hash.


### Attributes that conflict with ghost methods

There may be cases where you need to specify an attribute
that also happens to be a method that already exists on the ghost object.
In this case, simply call the attribute method explicitly.

	ghost :child do
	  attribute :attribute_names
	end


Matcher
-------

The matcher is used to specify the hash/array to test and what ghost to use.


### Basic Use

	expect(hash).to mirror_ghost :foo

In this case, hash is the output that is under test.
A ghost named foo is expected to be defined and a reference object named foo
is also expected to be defined (presumably via a let statement).


### Specifying the refencence object

If for any reason, it is necessary to specify the reference object
instead of relying on a name that is contextually available to the matcher,
it may be explicitly passed in:

	expect(hash).to mirror_ghost(:foo).for reference_object


### Strict mode

By using strict mode, no nil values are allowed. Ever. Anywhere.
This gives the user the ability to ensure that everything
is coming through as expected.
It is not uncommon to think everything is working
when there are a bunch of nil values representing computed values
that turn out to be computed incorrectly.
Strict mode gives confidence that all attributes have expected values.

	expect(hash).to mirror_ghost(:foo).strictly


### Reference and Strict

Specifying a reference and setting strict mode may be chained:

	expect(hash).to mirror_ghost(:foo).for(reference_object).strictly


Additional Information
----------------------

The [main repo][peppers-ghost] is on Bitbucket, NOT on Github.
Please open issues and make pull requests there.


Credits
-------

Pepper's Ghost would not exist without valuable contributions from:
[Rudy Jachcan][rudy]
[Patrick Collins][patrick]


[peppers-ghost]: https://bitbucket.org/ToadJamb/peppers-ghost
[rudy]:          https://github.com/rudyjahchan
[patrick]:       https://github.com/patrick99e99
